#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import pynotify
import time
import os
import urllib
import urllib2
import feedparser

# Main
def main():

#------configurations-----------
	
	#trusted users
	users={'beygi','imool'}
	#guest users
	guest='isfdguest'

	#timeouts in second
	notify_timeout=5
	refresh_timeout=10
#-----end of configuration------

	


	#initial definitions
	startup=1
	avatars={}
	show={}
	print os.getcwd()
	pynotify.init("Dent2Notify")
	while True:
		for user in users:
			print "Reading "+user+"'s favorites feed"
			#read favorites from feed
			dents=feedparser.parse("https://identi.ca/api/favorites/"+user+".rss")["entries"]
			
			for dent in dents:
				#fetch sender avatar from notice feed
				notice_id=dent["link"].split('/')[-1].split('#')[0].split('?')[0]
				if(startup):
					#all dents will be ignored in startup
					show[notice_id]=1
				elif(not notice_id in show):
					#fetch avatar if it dosn't exist in our cache and display new dent via libnotify
					user_detail=feedparser.parse("https://identi.ca/api/statuses/show/"+notice_id+".atom");
					url=user_detail["entries"][0]['links'][3]['href'];
					filename = url.split('/')[-1].split('#')[0].split('?')[0]
					if(not dent["title_detail"]["value"].split(':')[0] in avatars):
						print 'fetching avatar : '+dent["title_detail"]["value"].split(':')[0]
						urllib.urlretrieve(url, os.getcwd()+"/"+filename)
						avatars[dent["title_detail"]["value"].split(':')[0]]=filename
					notice=dent["title_detail"]["value"].split(':')
					notice.pop(0)
					notice=':'.join(notice);

	
					#if dent sender is guest , so look at mobile folder for avatar
					if(dent["title_detail"]["value"].split(':')[0]==guest):
						dent["title_detail"]["value"]="مهمان همایش"
						#look at online database for mobile number with dent id
						print "guest user detected , try to find avatar in mobiles folder"
						
						#web site url must tell us mobile number of a dent id
						usock = urllib2.urlopen('http://yourserver.com/sms/?dent='+notice_id)
						mobile = usock.read()
						print "mobile number is: "+mobile
						usock.close()
						if(os.path.exists(os.getcwd()+"/mobiles/avatars/"+mobile+".jpg")):
							filename="/mobiles/avatars/"+mobile+".jpg"
						
											
					print "Display notification , dent id: "+notice_id+" from: "+dent["title_detail"]["value"].split(':')[0]+" content: "+notice
					n = pynotify.Notification(dent["title_detail"]["value"].split(':')[0],notice,os.getcwd()+"/"+filename)
					n.set_urgency(pynotify.URGENCY_CRITICAL)
					n.set_timeout(notify_timeout)
					n.show()
					show[notice_id]=1
					#delay between notifications
					time.sleep(notify_timeout+1)
		startup=0
		#delay between server requests
		time.sleep(refresh_timeout)
main()
